import ceylon.test {
    ...
}

[Element*] dropEveryNth<Element>(Integer nth, {Element*} list)
given Element satisfies Object {
    return list.indexed
        .filter((Integer index->Element elem) => (index + 1) % nth != 0)
        .map((Integer index->Element elem) => elem)
        .sequence();
}

test
shared void should_drop_every_nth() {
    value list = "abcdefghijk";
    value expected = "abdeghjk";
    assertEquals(dropEveryNth(3, list), expected.sequence());
}