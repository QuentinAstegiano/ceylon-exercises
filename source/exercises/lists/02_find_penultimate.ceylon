import ceylon.test {
    ...
}

shared Element? penultimate<Element>({Element*} list) {
    return list.exceptLast.last;
}

test
shared void should_find_penultimate_element_of_a_list() {
    value list = [1, 1, 2, 3, 5, 8];
    assert (exists elt = penultimate(list));
    assertEquals(elt, 5);
}