import ceylon.test {
    ...
}

[Element*] decode<Element>([Element|[Integer, Element]*] list)
    given Element satisfies Object =>
        // convert all groups to a single list of Element
        list.flatMap<Element,Null>((Element|[Integer, Element] group) =>
                // special case: only one element
                if (is Element group) then [group]
                // standard case: one element repeated N times
                else [group[1]].repeat(group[0]))
            .sequence();

test
shared void should_decode_length_encoded_list() {
    value list = [[4, 'a'], 'b', [2, 'c'], [2, 'a'], 'd', [4, 'e']];
    value expected = "aaaabccaadeeee";
    assertEquals(decode(list), expected.sequence());
}