import ceylon.test {
    ...
}

{[Element*]*} distinctCombinations<Element>(Integer setSize, {Element*} list)
given Element satisfies Object =>
    if (exists first = list.first, !list.rest.empty, setSize>1) then
        // if the list is not empty, have more than one element, and set size > 1
        //  add the first element to each of the combinations generated
        //  with the rest of the list and a decremented size
        //  eg. "a" / "bc" => "ab" / "ac"
        distinctCombinations(setSize - 1, list.rest)
            .map<[Element*]>((Element[] element) => [first].append(element))
        // append to that partial list each of the combinations generated
        //  with the rest of the list and the given size
            .sequence().append(distinctCombinations(setSize, list.rest).sequence())
    else
        // if the set size is 1, then the result list if a list of each element
        //  eg "bc" => "b", "c"
        list.flatMap<[Element*],Null>((Element element) => [[element]]).sequence();


shared void should_find_no_combination_when_set_size_is_0() {
    value list = "abc";
    value expected = [];

    assertEquals(distinctCombinations(0, list), expected);
}

shared void should_find_one_combination_by_element_when_set_size_is_1() {
    value list = "abc";
    value expected = ["a", "b", "c"];

    value actual = distinctCombinations(1, list);
    assertEquals(actual.size, expected.size);
    assertTrue(expected.every((String elem) => actual.contains(elem.sequence())));
}

shared void should_find_three_combinations_when_set_size_is_2_and_list_size_is_3() {
    value list = "abc";
    value expected = ["ab", "ac", "bc"];

    value actual = distinctCombinations(2, list);
    assertEquals(actual.size, expected.size);
    assertTrue(expected.every((String elem) => actual.contains(elem.sequence())));
}

shared void should_find_all_combinations() {
    value list = "abcde";
    value expected = ["abc", "abd", "abe", "acd", "ace", "ade", "bcd", "bce", "bde", "cde"];

    value actual = distinctCombinations(3, list);
    assertEquals(actual.size, expected.size);
    assertTrue(expected.every((String elem) => actual.contains(elem.sequence())));
}