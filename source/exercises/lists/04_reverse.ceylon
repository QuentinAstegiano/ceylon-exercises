import ceylon.test {
    ...
}

shared {Element*} reverse<Element>({Element*} list) {
    if (list.size == 1) {
        return list;
    } else {
        return reverse(list.rest).chain([list.first].coalesced);
    }
}

test
shared void should_reverse_list() {
    value list = [1, 1, 2, 3, 5, 8];
    assertEquals(reverse(list).sequence(), [8, 5, 3, 2, 1, 1]);
}