import ceylon.test {
    ...
}

shared Element? last<Element>({Element*} list) {
    return list.last;
}

test
shared void should_find_last_element_of_a_list() {
    value list = [1, 1, 2, 3, 5, 8];
    assert (exists elt = last(list));
    assertEquals(elt, 8);
}

