import ceylon.test {
    ...
}

[Element|Null|[Integer, Element]*] encodeModified<Element>({Element*} list)
        given Element satisfies Object {
    return pack(list)
        // map each group
        .map<Element|Null|[Integer, Element]>(({Element*} group) =>
            // if the group is not empty
            if (exists first = group.first) then
                // if there is only one element in the group, then we keep only the element
                if (group.size == 1) then first
                // if not, we also keep it's size
                else [group.size, first]
            // if the group is empty
            else null)
        .sequence();
}

test
shared void shoud_length_encode_list_with_single_element_simplification() {
    value list = "aaaabccaadeeee";
    value expected = [[4, 'a'], 'b', [2, 'c'], [2, 'a'], 'd', [4, 'e']];
    assertEquals(expected, encodeModified(list));
}