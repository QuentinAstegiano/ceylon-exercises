import ceylon.test {
    ...
}

shared Element? nth<Element>(Integer index, {Element*} list) {
    return list.getFromFirst(index);
}

test
shared void should_find_nth_element_of_a_list() {
    value list = [1, 1, 2, 3, 5, 8];
    assert (exists elt = nth(2, list));
    assertEquals(elt, 2);
}