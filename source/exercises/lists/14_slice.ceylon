import ceylon.test {...}

[Element*] slice<Element>(Integer startAt, Integer endAt, {Element*} list)
given Element satisfies Object =>
    list.take(endAt).skip(startAt).sequence();

test
shared void should_extract_slice() {
    value list = "abcdefghijk";
    value expected = "defg";
    assertEquals(slice(3, 7, list), expected.sequence());
}