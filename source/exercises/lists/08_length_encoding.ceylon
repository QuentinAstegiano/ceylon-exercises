import ceylon.test {
    ...
}

[Null|[Integer, Element]*] encode<Element>({Element*} list)
        given Element satisfies Object {
    return pack(list)
        .map<Null|[Integer, Element]>(({Element*} group) =>
            if (exists first = group.first) then [group.size, first]
            else null)
        .sequence();
}

test
shared void shoud_length_encode_list() {
    value list = "aaaabccaadeeee";
    value expected = [[4, 'a'], [1, 'b'], [2, 'c'], [2, 'a'], [1, 'd'], [4, 'e']];
    assertEquals(expected, encode(list));

}