import ceylon.test {
    ...
}

[Element*] duplicate<Element>(Integer times, {Element*} list)
given Element satisfies Object =>
    list.flatMap<Element, Null>((Element element) => [element].repeat(times))
        .sequence();

test
shared void should_duplicate_elements() {
    value list = "abcde";
    value expected = "aaabbbcccdddeee";
    assertEquals(duplicate(3, list), expected.sequence());
}