import ceylon.test {
    ...
}

shared Boolean isPalindrome<Element>({Element*} list) {
    if (list.size<=1) {
        return true;
    } else if (exists first = list.first, exists last = list.last, first == last) {
        return isPalindrome(list.rest.exceptLast);
    } else {
        return false;
    }
}

test
shared void should_find_is_list_is_a_palindrome() {
    value list = [1, 1, 2, 3, 5, 8];
    assertTrue(!isPalindrome(list));

    value list2 = [1, 2, 3, 2, 1];
    assertTrue(isPalindrome(list2));

    value list3 = [1, 2, 2, 1];
    assertTrue(isPalindrome(list3));
}