import ceylon.test {
    ...
}

shared {Element*} removeConsecutiveDuplicates<Element>({Element*} list) =>
    list.fold<{Element*}>({})(({Element*} partial, Element elem) =>
        // if there is a last element that is the same that the current one, then skip it
        if (exists last = partial.last, exists elem, last == elem) then partial
        // else add it to the partial result
        else partial.sequence().withTrailing(elem));

test
shared void should_remove_consecutive_duplicates_from_list() {
    value list = "aaaabccaadeeee";
    value expected = ['a', 'b', 'c', 'a', 'd', 'e'];
    value actual = removeConsecutiveDuplicates(list).sequence();
    assertEquals(actual, expected);
}