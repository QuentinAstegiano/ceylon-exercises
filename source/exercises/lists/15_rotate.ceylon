import ceylon.test {
    ...
}

[Element*] rotateLeft<Element>(Integer rotation, {Element*} list)
given Element satisfies Object =>
    if (rotation.positive) then list.skip(rotation).sequence().append(list.take(rotation).sequence())
    else rotateLeft(list.size + rotation, list);

shared void should_left_rotate_list() {
    value list = "abcdefghijk";
    value expected = "defghijkabc";
    assertEquals(rotateLeft(3, list), expected.sequence());
}

shared void should_left_rotate_list_with_negative_index() {
    value list = "abcdefghijk";
    value expected = "jkabcdefghi";
    assertEquals(rotateLeft(-2, list), expected.sequence());
}

shared void should_not_rotate_list_when_rotation_is_0() {
    value list = "abcdefghijk";
    value expected = "abcdefghijk";
    assertEquals(rotateLeft(0, list), expected.sequence());
}