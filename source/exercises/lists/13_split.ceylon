import ceylon.test {
    ...
}

[Element*][2] split<Element>(Integer limit, {Element*} list)
given Element satisfies Object =>
    [list.take(limit).sequence(), list.skip(limit).sequence()];

test
shared void should_split_list() {
    value list = "abcdefghijk";
    value expected = ["abc".sequence(), "defghijk".sequence()];
    assertEquals(split(3, list), expected);
}