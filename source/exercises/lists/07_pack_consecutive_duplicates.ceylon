import ceylon.test {
    ...
}

[[Element*]*] pack<Element>({Element*} list)
        given Element satisfies Object {
    // If the list is not empty
    if (exists first = list.first) {
        // take all the duplicated elements
        value group = list.takeWhile((Element elem) => elem == first).sequence();
        // and recursively build a list of each groups
        return [group].append(pack(list.skip(group.size)));
    } else {
        return [];
    }
}

test
shared void should_pack_duplicates_into_sublist() {
    value list = "aaaabccaadeeee";
    value expected = [['a', 'a', 'a', 'a'], ['b'], ['c', 'c'], ['a', 'a'], ['d'], ['e', 'e', 'e', 'e']];
    assertEquals(pack(list), expected);
}