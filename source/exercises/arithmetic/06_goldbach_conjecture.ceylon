import ceylon.test {
    ...
}

[Integer, Integer] goldbachConjecture(Integer n) {

    if (exists firstHalf = (2 .. n).find((Integer i) => isPrime(i) && isPrime(n - i))) {
        return [firstHalf, n - firstHalf];
    } else {
        // ??
        return nothing;
    }
}

test
shared void should_find_goldbach_conjecture_composition() {
    assertEquals(goldbachConjecture(4), [2, 2]);
    assertEquals(goldbachConjecture(6), [3, 3]);
    assertEquals(goldbachConjecture(8), [3, 5]);
    assertEquals(goldbachConjecture(10), [3, 7]);
    assertEquals(goldbachConjecture(12), [5, 7]);
    assertEquals(goldbachConjecture(28), [5, 23]);
}