import ceylon.test {
    ...
}

{<Integer -> Integer[2]>*} goldbachList(Integer start, Integer end) {
    return (start .. end)
        .filter((Integer i) => i.even)
        .map<Integer->Integer[2]>((Integer i) {
             value composition = goldbachConjecture(i);
             return i -> [composition[0], composition[1]];
        });
}

test
shared void should_print_goldbach_conjecture_list() {
    goldbachList(9, 20).each((Integer i->Integer[2] composition) =>
        print("``i`` = ``composition[0]`` + ``composition[1]``"));
}