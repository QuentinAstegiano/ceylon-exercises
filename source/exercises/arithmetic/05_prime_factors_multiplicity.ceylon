import ceylon.test {
    ...
}

[[Integer, Integer]*]|Null primeFactorsWithMultiplicity(Integer int) =>
        if (exists factors = primeFactors(int)) then factors
            .group<Integer>((Integer i) => i)
            .map<[Integer, Integer]>((Integer key->[Integer+] group) => [key, group.size])
            .sequence()
        else null;

test
shared void should_find_prime_factor_with_multiplicity_list() {
    assertEquals(primeFactorsWithMultiplicity(315), [[3, 2], [5, 1], [7, 1]]);
}