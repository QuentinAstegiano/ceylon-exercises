import ceylon.test {
    ...
}

Integer findGreatestDivisor(Integer n1, Integer n2) =>
    if (n1 == n2) then n1
    else if (n2 > n1) then findGreatestDivisor(n1, n2 - n1)
    else findGreatestDivisor(n2, n1 - n2);

shared void should_find_greatest_divisor() {
    assertEquals(findGreatestDivisor(1, 2), 1);
    assertEquals(findGreatestDivisor(36, 63), 9);
}