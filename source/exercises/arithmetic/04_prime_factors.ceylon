import ceylon.test {
    ...
}
import ceylon.math.float {
    sqrt
}

[Integer+]|Null primeFactors(Integer int) {

    [Integer+]|Null findAllPrimeFactors(Integer n, Integer startAt) {
        if (isPrime(n)) {
            return [n];
        } else {
            value factor = (startAt..sqrt(n.float).integer).sequence()
                .find((Integer i) => isPrime(i) && n % i == 0);
            if (exists factor, exists next = findAllPrimeFactors(n / factor, factor)) {
                return [factor].append(next);
            } else {
                return null;
            }
        }
    }

    return findAllPrimeFactors(int, 2);
}

test
shared void should_find_prime_factor_list() {
    assertEquals(primeFactors(315), [3, 3, 5, 7]);
}