import ceylon.test {
    ...
}

Boolean isPrime(Integer number) =>
    number == 2 || !(2..number-1).any((Integer i) => number % i == 0);

shared void should_assert_that_not_prime_number_are_not_primes() {
    assertFalse(isPrime(1));
    assertFalse(isPrime(4));
    assertFalse(isPrime(10));
}

shared void should_assert_that_prime_number_are_primes() {
    assertTrue(isPrime(2));
    assertTrue(isPrime(3));
    assertTrue(isPrime(7));
}