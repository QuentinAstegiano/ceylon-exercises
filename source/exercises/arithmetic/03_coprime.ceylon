import ceylon.test {
    ...
}

Boolean areCoprime(Integer n1, Integer n2) {
    {Integer*} getDivisors(Integer n) {
        return (2..n).filter((Integer div) => n % div == 0);
    }

    {Integer*} n1Div = getDivisors(n1);
    {Integer*} n2Div = getDivisors(n2);

    return !n1Div.any(n2Div.contains) && !n2Div.any(n1Div.contains);
}

test
shared void should_find_numbers_are_not_coprime() {
    assertFalse(areCoprime(14, 21));
}

test
shared void should_find_numbers_are_coprime() {
    assertTrue(areCoprime(14, 15));
    assertTrue(areCoprime(35, 64));
}