import ceylon.test {
    ...
}
import ceylon.file {
    ...
}
import ceylon.time {
    systemTime
}

[[String*]*] allAnagramsInListOptimized([String*] list) {
    return list
            .map((String s) => s.lowercased)
            .distinct
            .group((String s) => s.sort((Character x, Character y) => x.compare(y)))
            .map<[String*]>((Character[] group -> [String+] list) => list)
            .filter((String[] list) => list.size > 1)
            .sequence();
}

test
shared void should_find_anagrams_in_list_optimized() {
    value list = ["boaster", "hello", "fresher", "sort", "boaters", "john", "rots", "refresh", "borates"];
    value expected = [["boaster", "boaters", "borates"],
        ["fresher", "refresh"],
        ["sort", "rots"]];
    value actual = allAnagramsInListOptimized(list);
    assertEquals(actual.size, expected.size);
    expected.every(actual.contains);
}

test
shared void should_find_anagrams_from_file() {
    Resource file = parsePath("resource/wordlist.txt").resource;
    if (is File file) {
        value startReadfile = systemTime.instant();
        value list = lines(file);
        print("Read ``list.size`` lines in ``systemTime.instant().durationFrom(startReadfile)``");

        value start = systemTime.instant();
        value anagrams = allAnagramsInListOptimized(list);
        print("Found ``anagrams.size`` groups in ``systemTime.instant().durationFrom(start)``");

        anagrams
            .sort((String[] x, String[] y) => x.size.compare(y.size))
            .each(print);
    }
}
