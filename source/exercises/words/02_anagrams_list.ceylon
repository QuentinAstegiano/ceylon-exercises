import ceylon.test {
    ...
}

[[String*]*] allAnagramsInList([String*] list) {
    if (exists first = list.first) {
        value anagrams = list.rest.filter((String other) => areAnagrams(first, other));
        if (!anagrams.empty) {
            return [[first].append(anagrams.sequence())]
                .append(allAnagramsInList(list.rest.filter((String s) => !anagrams.contains(s)).sequence()));
        } else {
            return allAnagramsInList(list.rest);
        }
    } else {
        return [];
    }
}

test
shared void should_find_anagrams_in_list() {
    value list = ["boaster", "hello", "fresher", "sort", "boaters", "john", "rots", "refresh", "borates"];
    value expected = [["boaster", "boaters", "borates"],
        ["fresher", "refresh"],
        ["sort", "rots"]];
    value actual = allAnagramsInList(list);
    assertEquals(actual.size, expected.size);
    expected.every(actual.contains);
}