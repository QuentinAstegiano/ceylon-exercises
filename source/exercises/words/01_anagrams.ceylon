import ceylon.test {
    ...
}

Boolean areAnagrams(String word, String other) {
    return word.size == other.size &&
        word.lowercased.every(other.lowercased.contains) &&
        other.lowercased.every(word.lowercased.contains);
}

test
shared void should_find_two_words_are_not_anagrams() {
    assertFalse(areAnagrams("hello", "John"));
    assertFalse(areAnagrams("hello", "world"));
}

test
shared void should_find_two_words_are_anagrams() {
    assertTrue(areAnagrams("rots", "sort"));
    assertTrue(areAnagrams("fresher", "refresh"));
}